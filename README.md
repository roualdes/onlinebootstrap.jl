# OnlineBootstrap

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://roualdes.gitlab.io/OnlineBootstrap.jl/)
[![Build Status](https://gitlab.com/roualdes/OnlineBootstrap.jl/badges/main/pipeline.svg)](https://gitlab.com/roualdes/OnlineBootstrap.jl/pipelines)
[![Coverage](https://gitlab.com/roualdes/OnlineBootstrap.jl/badges/main/coverage.svg)](https://gitlab.com/roualdes/OnlineBootstrap.jl/commits/main)

An implementation of the Bootstrap that doesn't scale with the sample size.

# TODO

* Docs
* Examples
* Incorporate [OnlineStats.StatLearn](https://joshday.github.io/OnlineStats.jl/latest/api/#OnlineStats.StatLearn) to "fit a model (via stochastic approximation) that is linear in the parameters."


# References

* [An introduction to the Poisson bootstrap](https://www.unofficialgoogledatascience.com/2015/08/an-introduction-to-poisson-bootstrap26.html) by Amir Najmi

* [Estimating Uncertainty for Massive Data Streams](https://research.google/pubs/pub43157/) by Nicholas Chamandy, Omkar Muralidharan, Amir Najmi, and Siddartha Naidu

* [Creating non-parametric bootstrap samples using
Poisson frequencies](http://www.med.mcgill.ca/epidemiology/hanley/reprints/bootstrap-hanley-macgibbon2006.pdf) by James A. Hanley and Brenda MacGibbon

* [Statistic Estimators](https://anthonylloyd.github.io/blog/2021/11/01/statistic-estimators) by Anthony Lloyd

* [The P2 Algorithm for Dynamic Statistical Computing Calculation of Quantiles and Histograms Without Storing Observations](https://anthonylloyd.github.io/public/stats/psqr.pdf) by Raj Jain and Imrich Chlamtac
