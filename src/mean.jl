struct OnlineBootMean{T <: AbstractFloat} <: AbstractOnlineBoot
    m::Vector{T}
    n::Vector{Int}
end

function OnlineBootMean(T, R)
    return OnlineBootMean(zeros(T, R),
                          zeros(Int, R))
end

OnlineBootMean(R) = OnlineBootMean(Float64, R)

function accumulateob!(ob::OnlineBootMean, x, r)
    m = x
    m -= ob.m[r]
    ob.n[r] += 1
    ob.m[r] += m / ob.n[r]
end
