function boot(data, f::F, R = 301) where {F <: Function}
    N = length(data)
    ts = zeros(eltype(data), R)
    nt = Threads.nthreads()
    @sync for it in 1:nt
        Threads.@spawn for r in it:nt:R
            idx = rand(1:N, N)
            ts[r] = f(data[idx, :])
        end
    end
    return ts
end

function boot!(ob::AbstractOnlineBoot, x)
    R = length(ob.n)
    nt = Threads.nthreads()
    ps = randp(R)
    @sync for it in 1:nt
        Threads.@spawn for r in it:nt:R
            while ps[r] > 0
                accumulateob!(ob, x, r)
                ps[r] -= 1
            end
        end
    end
end
