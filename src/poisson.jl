function randp(N)
    xs = zeros(Int, N)
    px = exp(-1)
    ps = repeat([px], N)
    ss = copy(ps)
    us = rand(N)
    for n in eachindex(xs, us, ss)
        while us[n] > ss[n]
            xs[n] += 1
            ps[n] /= xs[n]
            ss[n] += ps[n]
        end
    end
    return xs
end

function randp()
    ps = randp(1)
    return ps[1]
end
