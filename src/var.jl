struct OnlineBootVar{T <: AbstractFloat} <: AbstractOnlineBoot
    m::Vector{T}
    v::Vector{T}
    n::Vector{Int}
end

function OnlineBootVar(T, R)
    return OnlineBootVar(zeros(T, R),
                              zeros(T, R),
                              zeros(Int, R))
end

OnlineBootVar(R) = OnlineBootVar(Float64, R)

function accumulateob!(ob::OnlineBootVar, x, r)
    m = x
    v = zero(x)

    m -= ob.m[r]
    v -= ob.v[r]

    ob.n[r] += 1
    w = 1 / ob.n[r]

    ob.m[r] += w * m
    ob.v[r] += w * v + w * (1 - w) * m ^ 2
end
