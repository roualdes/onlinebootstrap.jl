using OnlineBootstrap
using Documenter

DocMeta.setdocmeta!(OnlineBootstrap, :DocTestSetup, :(using OnlineBootstrap); recursive=true)

makedocs(;
    modules=[OnlineBootstrap],
    authors="Edward A. Roualdes <eroualdes@csuchico.edu> and contributors",
    repo="https://gitlab.com/roualdes/OnlineBootstrap/blob/{commit}{path}#{line}",
    sitename="OnlineBootstrap.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://roualdes.gitlab.io/OnlineBootstrap",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
