```@meta
CurrentModule = OnlineBootstrap
```

# OnlineBootstrap

Documentation for [OnlineBootstrap](https://gitlab.com/roualdes/OnlineBootstrap.jl).

```@index
```

```@autodocs
Modules = [OnlineBootstrap]
```
